let app = new Vue({
    el: '#app',
    data: {
        available: [],
        pickedPiece: {},
        victim: [],
        board: [],
        piesesInfo: []
    },

    mounted() {
        this.board[0] = []
        for (let x = 0; x < 8; x++) {
            this.board[x] = []
            for (let y = 0; y < 8; y++) {
                if ((x < 2) && ((x % 2) != (y % 2))) {
                    this.board[x][y] = 'black'
                    this.piesesInfo.push({ x, y, piece: 'black' })

                } else if ((x > 5) && ((x % 2) != (y % 2))) {
                    this.board[x][y] = 'white'
                    this.piesesInfo.push({ x, y, piece: 'white' })

                } else {
                    this.board[x][y] = 'empty'
                }
            }
        }
    },

    methods: {
        getInfo(x, y) {
            let info = this.piesesInfo.find(el => el.x == x && el.y == y)
            return info || {};
        },

        getAvailable(x, y) {
            let res = {
                have: this.available.some(el => el.x == x && el.y == y),
                msg: (this.available.find(el => el.x == x && el.y == y) || {}).msg
            }
            return res
        },


        moveFrom(x, y, piece) {
            this.available = []
            this.pickedPiece = { x, y, piece };

            let lt = ((x - 1) >= 0 && (y - 1) >= 0) ? { x: x - 1, y: y - 1, piece: this.board[x - 1][y - 1] } : null
            let rt = ((x - 1) >= 0 && (y + 1) <= 7) ? { x: x - 1, y: y + 1, piece: this.board[x - 1][y + 1] } : null
            let rb = ((x + 1) <= 7 && (y + 1) <= 7) ? { x: x + 1, y: y + 1, piece: this.board[x + 1][y + 1] } : null
            let lb = ((x + 1) <= 7 && (y - 1) >= 0) ? { x: x + 1, y: y - 1, piece: this.board[x + 1][y - 1] } : null

            if (rt && rt.piece == 'empty') this.available.push({ ...rt, msg: 'Пойти' })
            if (rb && rb.piece == 'empty') this.available.push({ ...rb, msg: 'Пойти' })
            if (lt && lt.piece == 'empty') this.available.push({ ...lt, msg: 'Пойти' })
            if (lb && lb.piece == 'empty') this.available.push({ ...lb, msg: 'Пойти' })

            if (piece == 'white') {
                if (rt && rt.piece == "black" && this.board[x - 2][y + 2] == 'empty') {
                    this.available.push({ x: x - 2, y: y + 2, msg: 'Съесть!' })
                    this.victim.push({ x: x - 1, y: y + 1, piece: this.board[x - 1][y + 1], if: { x: x - 2, y: y + 2 } })
                }
                if (rb && rb.piece == "black" && this.board[x + 2][y + 2] == 'empty') {
                    this.available.push({ x: x + 2, y: y + 2, msg: 'Съесть!' })
                    this.victim.push({ x: x + 1, y: y + 1, piece: this.board[x + 1][y + 1], if: { x: x + 2, y: y + 2 } })
                }
                if (lt && lt.piece == "black" && this.board[x - 2][y - 2] == 'empty') {
                    this.available.push({ x: x - 2, y: y - 2, msg: 'Съесть!' })
                    this.victim.push({ x: x - 1, y: y - 1, piece: this.board[x - 1][y - 1], if: { x: x - 2, y: y - 2 } })
                }
                if (lb && lb.piece == "black" && this.board[x + 2][y - 2] == 'empty') {
                    this.available.push({ x: x + 2, y: y - 2, msg: 'Съесть!' })
                    this.victim.push({ x: x + 1, y: y - 1, piece: this.board[x + 1][y - 1], if: { x: x + 2, y: y - 2 } })
                }
            } else if (piece == 'black') {
                if (rt && rt.piece == "white" && this.board[x - 2][y + 2] == 'empty') {
                    this.available.push({ x: x - 2, y: y + 2, msg: 'Съесть!' })
                    this.victim.push({ x: x - 1, y: y + 1, piece: this.board[x - 1][y + 1], if: { x: x - 2, y: y + 2 } })
                }
                if (rb && rb.piece == "white" && this.board[x + 2][y + 2] == 'empty') {
                    this.available.push({ x: x + 2, y: y + 2, msg: 'Съесть!' })
                    this.victim.push({ x: x + 1, y: y + 1, piece: this.board[x + 1][y + 1], if: { x: x + 2, y: y + 2 } })
                }
                if (lt && lt.piece == "white" && this.board[x - 2][y - 2] == 'empty') {
                    this.available.push({ x: x - 2, y: y - 2, msg: 'Съесть!' })
                    this.victim.push({ x: x - 1, y: y - 1, piece: this.board[x - 1][y - 1], if: { x: x - 2, y: y - 2 } })
                }
                if (lb && lb.piece == "white" && this.board[x + 2][y - 2] == 'empty') {
                    this.available.push({ x: x + 2, y: y - 2, msg: 'Съесть!' })
                    this.victim.push({ x: x + 1, y: y - 1, piece: this.board[x + 1][y - 1], if: { x: x + 2, y: y - 2 } })
                }
            }

            // axios.get('http://localhost:8888/start', { params: { x, y, piece } }).then(response => {
            //     this.available = response.data.available
            // })
        },

        moveTo(x, y, piece) {
            this.piesesInfo = this.piesesInfo
                .map(el => el.x == this.pickedPiece.x && el.y == this.pickedPiece.y ? { x, y, piece } : el)

            this.available = []
            this.board[this.pickedPiece.x][this.pickedPiece.y] = 'empty'
            this.board[x][y] = piece

            if (Math.abs(x - this.pickedPiece.x) == 2 && Math.abs(y - this.pickedPiece.y) == 2) {
                let victim = this.victim.find(el => el.if.x == x && el.if.y == y)
                this.piesesInfo = this.piesesInfo
                    .map(el => el.x == victim.x && el.y == victim.y ? {} : el)
                this.board[victim.x][victim.y] = 'empty'
            }

            this.pickedPiece = {}
            this.victim = []

            // axios.get('http://localhost:8888/move', { params: { x, y, piece } }).then(response => {
            //     this.available = response.data.available
            // })

        }
    },
    computed: {
        stats() {
            let white = this.piesesInfo.filter(el => el.piece == 'white').length
            let black = this.piesesInfo.filter(el => el.piece == 'black').length
            return { white, black }
        }
    },
})